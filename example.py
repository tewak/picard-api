from PicardApi import PicardApi
from pprint import pprint
picard = PicardApi('devcardinal')
# Login as user
picard.login()

# Make API call to /stackId
# If no HTTP method is specified the query function defaults to GET
stack_response = picard.query('/admin/stackId')
pprint({"http_status_code": stack_response.status_code})
pprint(stack_response.json)

# To specify request parameters you can specify a variable named params
# For example, to get the first 2 doctypes from the /custom/doctypeList endpoint
doctypes_response = picard.query("/custom/doctypeList", params={"limit": 2})
pprint({"http_status_code": doctypes_response.status_code})
pprint(doctypes_response.json)

# And to specify the request method you can set the variable named method to one of 'post', 'get', 'put', or 'delete'
# For example, to create a new document for the first doctype returned in the previous call you can run
new_document_params = {
    'doctype': doctypes_response.json['doctypes'][0]['id'],
    'document': {
        'field1': 'value1',
        'field2': 2
    }
}
document_response = picard.query("/custom/document", method="post", params=new_document_params)
pprint({"http_status_code": document_response.status_code})
pprint(document_response.json)
